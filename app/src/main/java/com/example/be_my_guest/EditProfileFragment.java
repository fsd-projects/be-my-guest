package com.example.be_my_guest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.be_my_guest.databinding.FragmentEditProfileBinding;
import com.example.be_my_guest.model.UserModel;
import com.example.be_my_guest.model.entities.User;
import com.example.be_my_guest.viewModel.EditProfileViewModel;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.squareup.picasso.Picasso;

public class EditProfileFragment extends Fragment {
    private User signedUser;
    private EditText firstName;
    private EditText lastName;
    private ImageView userPhoto;
    private FragmentEditProfileBinding binding;
    private ActivityResultLauncher<Void> cameraLauncher;
    private ActivityResultLauncher<String> galleryLauncher;
    private EditProfileViewModel editProfileViewModel;
    private CircularProgressIndicator progressIndicator;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEditProfileBinding.inflate(inflater, container, false);
        initializeImageLaunchers();

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        initializeUserDetails();
        setImageClickListeners();
        setSaveButtonListener();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        editProfileViewModel = new ViewModelProvider(this).get(EditProfileViewModel.class);
    }

    private void setSaveButtonListener() {
        binding.editProfileSaveButton.setOnClickListener(view1 -> {
            binding.editProfileSaveButton.setEnabled(false);
            progressIndicator.show();
            updateUser();
        });
    }

    private void updateUser() {
        User newUser = initializeNewUser();
        Bitmap profileImage = ((BitmapDrawable) userPhoto.getDrawable()).getBitmap();

        UserModel.instance().uploadImage(newUser.getEmail(), profileImage, url -> {
            setUserImage(newUser, url);
            updateNewUser(newUser);
        });
    }

    private void setUserImage(User newUser, String url) {
        if (url != null) {
            newUser.setPhotoUrl(url);
        }
    }

    @NonNull
    private User initializeNewUser() {
        User newUser = new User(signedUser);
        newUser.setFirstName(firstName.getText().toString());
        newUser.setLastName(lastName.getText().toString());

        return newUser;
    }

    private void updateNewUser(User user) {
        editProfileViewModel.updateUser(user, () -> {
            binding.editProfileSaveButton.setEnabled(true);
            progressIndicator.hide();
            Navigation.findNavController(binding.getRoot()).popBackStack();
        });
    }

    private void setImageClickListeners() {
        binding.editProfileUploadFromCamera.setOnClickListener(view1 -> {
            cameraLauncher.launch(null);
        });

        binding.editProfileUploadFromGallery.setOnClickListener(view1 -> {
            galleryLauncher.launch("image/*");
        });
    }

    private void init() {
        firstName = binding.editProfileFirstName;
        lastName = binding.editProfileLastName;
        userPhoto = binding.editProfileFragmentPhoto;
        progressIndicator = binding.editProfileProgressIndicator;
    }

    private void initializeImageLaunchers() {
        cameraLauncher = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), result -> {
            if (result != null) {
                binding.editProfileFragmentPhoto.setImageBitmap(result);
            }
        });
        galleryLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(), result -> {
            if (result != null) {
                binding.editProfileFragmentPhoto.setImageURI(result);
            }
        });
    }

    private void initializeUserDetails() {
        signedUser = UserProfileFragmentArgs.fromBundle(getArguments()).getUser();
        firstName.setText(signedUser.getFirstName());
        lastName.setText(signedUser.getLastName());

        Picasso.get()
                .load(signedUser.getPhotoUrl())
                .into(userPhoto);
    }
}
