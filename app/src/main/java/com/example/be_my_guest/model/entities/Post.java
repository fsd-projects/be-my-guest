package com.example.be_my_guest.model.entities;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.be_my_guest.MyApplication;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.GeoPoint;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Entity
public class Post implements Serializable {

    @PrimaryKey
    @NonNull
    private String id;
    private String userId;
    private String image;
    private String description;
    private GeoPoint geoPoint;
    private String address;
    private Date postTime;
    private Date startDate;
    private Date endDate;
    private Long lastUpdated;

    public static final String COLLECTION = "Posts";
    public static final String ID = "id";
    public static final String USER_ID = "userId";
    public static final String IMAGE = "image";
    public static final String DESCRIPTION = "description";
    public static final String GEO_POINT = "geoPoint";
    public static final String ADDRESS = "address";
    public static final String POST_TIME = "postTime";
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    public static final String LAST_UPDATED = "lastUpdated";
    public static final String LOCAL_LAST_UPDATED = "users_local_last_update";

    public Post(@NonNull String id, String userId, String image, String description, GeoPoint geoPoint,
                String address, Date postTime, Date startDate, Date endDate) {
        this.id = id;
        this.userId = userId;
        this.image = image;
        this.description = description;
        this.geoPoint = geoPoint;
        this.address = address;
        this.postTime = postTime;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Post(Post post) {
        this.id = post.id;
        this.userId = post.userId;
        this.image = post.image;
        this.description = post.description;
        this.geoPoint = post.geoPoint;
        this.address = post.address;
        this.postTime = post.postTime;
        this.startDate = post.startDate;
        this.endDate = post.endDate;
        this.lastUpdated = post.lastUpdated;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getPostTime() {
        return postTime;
    }

    public void setPostTime(Date postTime) {
        this.postTime = postTime;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public static Post fromJson(Map<String, Object> json) {
        String id = (String) json.get(ID);
        String userId = (String) json.get(USER_ID);
        String image = (String) json.get(IMAGE);
        String description = (String) json.get(DESCRIPTION);
        GeoPoint geoPoint = (GeoPoint) json.get(GEO_POINT);
        String address = (String) json.get(ADDRESS);
        Date postTime = ((Timestamp)json.get(POST_TIME)).toDate();
        Date startDate = ((Timestamp)json.get(START_DATE)).toDate();
        Date endDate = ((Timestamp)json.get(END_DATE)).toDate();
        Post post = new Post(id, userId, image, description, geoPoint, address, postTime, startDate, endDate);
        try {
            Timestamp time = (Timestamp) json.get(LAST_UPDATED);
            post.setLastUpdated(time.getSeconds());
        } catch (Exception e) {
        }
        return post;
    }

    public Map<String, Object> toJson() {
        Map<String, Object> json = new HashMap<>();
        json.put(ID, getId());
        json.put(USER_ID, getUserId());
        json.put(IMAGE, getImage());
        json.put(DESCRIPTION, getDescription());
        json.put(GEO_POINT, getGeoPoint());
        json.put(ADDRESS, getAddress());
        json.put(POST_TIME, getPostTime());
        json.put(START_DATE, getStartDate());
        json.put(END_DATE, getEndDate());
        json.put(LAST_UPDATED, FieldValue.serverTimestamp());
        return json;
    }

    public static Long getLocalLastUpdate() {
        SharedPreferences sharedPref = MyApplication.getMyContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        return sharedPref.getLong(LOCAL_LAST_UPDATED, 0);
    }

    public static void setLocalLastUpdate(Long time) {
        SharedPreferences sharedPref = MyApplication.getMyContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(LOCAL_LAST_UPDATED,time);
        editor.commit();
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
