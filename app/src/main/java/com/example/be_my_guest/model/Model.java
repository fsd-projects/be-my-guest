package com.example.be_my_guest.model;

import com.example.be_my_guest.MyApplication;
import com.example.be_my_guest.room.AppDatabase;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public abstract class Model {
    protected Executor executor = Executors.newSingleThreadExecutor();

    AppDatabase localDb = AppDatabase.getInstance(MyApplication.getMyContext());

    public interface Listener<T>{
        void onComplete(T data);
    }

    public interface RefreshListener {
        void onComplete();
    }

    public enum LoadingState{
        LOADING,
        NOT_LOADING
    }
}
