package com.example.be_my_guest.model;

import android.graphics.Bitmap;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.be_my_guest.firebase.PostFirebaseModel;
import com.example.be_my_guest.model.entities.Post;
import com.example.be_my_guest.model.entities.relations.PostWithUser;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class PostModel extends Model {
    private static final PostModel _instance = new PostModel();
    private final PostFirebaseModel firebaseModel = new PostFirebaseModel();
    private final MutableLiveData<List<Post>> posts = new MutableLiveData<>();
    private final MutableLiveData<List<PostWithUser>> postsWithUser = new MutableLiveData<>();
    private final MutableLiveData<List<Post>> userPosts = new MutableLiveData<>();
    final public MutableLiveData<LoadingState> postsListLoadingState = new MutableLiveData<>(LoadingState.NOT_LOADING);
    final public MutableLiveData<LoadingState> postsWithUserListLoadingState = new MutableLiveData<>(LoadingState.NOT_LOADING);
    final public MutableLiveData<LoadingState> userPostsListLoadingState = new MutableLiveData<>(LoadingState.NOT_LOADING);

    public static PostModel instance() {
        return _instance;
    }

    public LiveData<List<PostWithUser>> getAllPostsWithUser() {
        if (postsWithUser.getValue() == null) {
            refreshAllPostsWithUser();
        }
        return postsWithUser;
    }

    public LiveData<List<Post>> getAllUserPosts() {
        if (userPosts.getValue() == null) {
            refreshUserPosts(() -> {});
        }

        return userPosts;
    }

    public void refreshAllPosts(RefreshListener listener) {
        postsListLoadingState.postValue(LoadingState.LOADING);
        Long localLastUpdate = Post.getLocalLastUpdate();
        firebaseModel.getAllPostsSince(localLastUpdate, list -> executor.execute(() -> {
            Long time = updatePostsInRoom(localLastUpdate, list);
            Post.setLocalLastUpdate(time);
            posts.postValue(localDb.postDao().getAll());
            postsListLoadingState.postValue(LoadingState.NOT_LOADING);
            listener.onComplete();
        }));
    }

    public void refreshAllPostsWithUser() {
        postsWithUserListLoadingState.postValue(LoadingState.LOADING);
        refreshUserPosts(() -> executor.execute(() -> {
            List<PostWithUser> postsList = localDb.postDao().getPostsWithUser();
            postsWithUser.postValue(postsList);
            postsWithUserListLoadingState.postValue(LoadingState.NOT_LOADING);
        }));
    }

    public void refreshUserPosts(RefreshListener listener) {
        userPostsListLoadingState.postValue(LoadingState.LOADING);
        UserModel.instance().refreshAllUsers(() -> refreshAllPosts(() -> executor.execute(() -> {
            List<Post> userPosts = localDb.postDao().getPostsByUserId(UserModel.instance().getCurrentUserId());
            this.userPosts.postValue(userPosts);
            userPostsListLoadingState.postValue(LoadingState.NOT_LOADING);
            listener.onComplete();
        })));
    }

    private Long updatePostsInRoom(Long localLastUpdate, List<Post> posts) {
        AtomicReference<Long> time = new AtomicReference<>(localLastUpdate);

        for (Post post : posts) {
            executor.execute(() -> {
                if (time.get() < post.getLastUpdated()) {
                    time.set(post.getLastUpdated());
                }
                localDb.postDao().insertAll(post);
            });
        }
        return time.get();
    }

    public void addPost(Post post) {
        firebaseModel.addPost(post, () -> {
            executor.execute(() -> localDb.postDao().insert(post));
            refreshAllPostsWithUser();
        });
    }

    public void uploadImage(String name, Bitmap bitmap, Listener<String> listener) {
        this.firebaseModel.uploadImage(name, bitmap, listener);
    }

    public void updatePost(Post post) {
        firebaseModel.updatePost(post, () -> executor.execute(() -> {
            localDb.postDao().update(post);
            refreshAllPostsWithUser();
        }));
    }
}
