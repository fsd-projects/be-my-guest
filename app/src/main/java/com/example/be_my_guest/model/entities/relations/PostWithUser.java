package com.example.be_my_guest.model.entities.relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.be_my_guest.model.entities.Post;
import com.example.be_my_guest.model.entities.User;

import java.io.Serializable;

public class PostWithUser implements Serializable {
    @Embedded
    public Post post;
    @Relation(
            parentColumn = "userId",
            entityColumn = "id")
    public User user;
}