package com.example.be_my_guest.model;

import android.graphics.Bitmap;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.be_my_guest.firebase.UserFirebaseModel;
import com.example.be_my_guest.model.entities.User;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class UserModel extends Model {
    private static final UserModel _instance = new UserModel();
    private final UserFirebaseModel firebaseModel = new UserFirebaseModel();
    private final MutableLiveData<User> profileUser = new MutableLiveData<>();
    private final MutableLiveData<List<User>> users = new MutableLiveData<>();
    final public MutableLiveData<LoadingState> usersListLoadingState = new MutableLiveData<>(LoadingState.NOT_LOADING);

    public static UserModel instance() {
        return _instance;
    }

    public String getCurrentUserId() {
        return firebaseModel.getCurrentUserUid();
    }

    public LiveData<User> getSignedUser() {
        if (profileUser.getValue() == null || !profileUser.getValue().getId().equals(getCurrentUserId())) {
            refreshAllUsers(() -> {});
        }

        return profileUser;
    }

    public void setSignedUser() {
        executor.execute(() -> {
            User user = localDb.userDao().findById(getCurrentUserId());
            if (user == null) {
                refreshAllUsers(() -> postProfileUser(localDb.userDao().findById(getCurrentUserId())));
            } else {
                PostModel.instance().refreshUserPosts(() -> postProfileUser(user));
            }
        });
    }

    private void postProfileUser(User user) {
        profileUser.postValue(user);
    }

    public void signUserOut() {
        executor.execute(() -> {
            postProfileUser(null);
        });
    }

    public void refreshAllUsers(RefreshListener listener) {
        usersListLoadingState.postValue(LoadingState.LOADING);
        Long localLastUpdate = User.getLocalLastUpdate();
        firebaseModel.getAllUsersSince(localLastUpdate, list -> executor.execute(() -> {
            Long time = updateUsersInRoom(localLastUpdate, list);
            User.setLocalLastUpdate(time);
            users.postValue(localDb.userDao().getAll());
            postProfileUser(localDb.userDao().findById(firebaseModel.getCurrentUserUid()));
            usersListLoadingState.postValue(LoadingState.NOT_LOADING);
            listener.onComplete();
        }));
    }

    private Long updateUsersInRoom(Long localLastUpdate, List<User> users) {
        AtomicReference<Long> time = new AtomicReference<>(localLastUpdate);

        for (User user : users) {
            executor.execute(() -> {
                if (time.get() < user.getLastUpdated()) {
                    time.set(user.getLastUpdated());
                }
                localDb.userDao().insertAll(user);
            });
        }
        return time.get();
    }


    public void addUser(User user, RefreshListener listener) {
        firebaseModel.addUser(user, (Void) -> {
            executor.execute(() -> {
                localDb.userDao().insert(user);
                refreshAllUsers(() -> {});
            });
            listener.onComplete();
        });
    }

    public void uploadImage(String name, Bitmap bitmap, Listener<String> listener) {
        this.firebaseModel.uploadImage(name, bitmap, listener);
    }

    public void updateUser(User user, RefreshListener listener) {
        firebaseModel.updateUser(user, (Void) -> {
            executor.execute(() -> {
                localDb.userDao().update(user);
                PostModel.instance().refreshAllPostsWithUser();
            });
            listener.onComplete();
        });
    }

    public boolean isSignedIn() {
        return firebaseModel.isSignedIn();
    }
}
