package com.example.be_my_guest;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.be_my_guest.databinding.FragmentSignInBinding;
import com.example.be_my_guest.utils.Utils;
import com.example.be_my_guest.viewModel.SignInViewModel;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;

public class SignInFragment extends Fragment {
    private FirebaseAuth mAuth;
    private EditText emailEdit;
    private EditText passwordEdit;
    private FragmentSignInBinding binding;
    private CircularProgressIndicator progressIndicator;
    private NavController navController;
    private SignInViewModel signInViewModel;
    private Button signInButton;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentSignInBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        initializeFields();
        setSignUpReferenceClickListener();
        setSignInClickListener();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        signInViewModel = new ViewModelProvider(this).get(SignInViewModel.class);
    }

    private void initializeFields() {
        emailEdit = binding.signInEmail;
        passwordEdit = binding.signInPassword;
        progressIndicator = binding.signInProgressIndicator;
        signInButton = binding.signInNextButton;
        mAuth = FirebaseAuth.getInstance();
    }

    private void setSignInClickListener() {
        binding.signInNextButton.setOnClickListener(view -> {
            if (Utils.validateEditTextFields(Arrays.asList(emailEdit, passwordEdit), getContext())) {
                signInButton.setEnabled(false);
                progressIndicator.show();
                SignUserIn();
            }
        });
    }

    private void setSignUpReferenceClickListener() {
        binding.signUpSignInReference.setOnClickListener(view -> {
            navController.navigate(R.id.action_sign_in_fragment_to_sign_up_fragment);
        });
    }

    private void SignUserIn() {
        mAuth.signInWithEmailAndPassword(emailEdit.getText().toString(), passwordEdit.getText().toString()).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                signIn();
            } else {
                showSignInFailure(task.getException());
            }
        });
    }

    private void showSignInFailure(Exception exception) {
        Log.w(TAG, "signInWithEmail:failure", exception);
        progressIndicator.hide();
        Toast.makeText(getContext(), "Authentication failed.", Toast.LENGTH_SHORT).show();
        signInButton.setEnabled(true);
    }

    private void signIn() {
        Log.d(TAG, "signInWithEmail:success");
        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        assert firebaseUser != null;
        signInViewModel.setSignedUser();
        startActivity(new Intent(MyApplication.getMyContext(), StartUpActivity.class));
    }
}