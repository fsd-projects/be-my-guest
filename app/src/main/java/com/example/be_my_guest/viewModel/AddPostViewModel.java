package com.example.be_my_guest.viewModel;

import android.graphics.Bitmap;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.be_my_guest.model.PostModel;
import com.example.be_my_guest.model.UserModel;
import com.example.be_my_guest.model.entities.Post;
import com.example.be_my_guest.model.entities.User;

public class AddPostViewModel extends ViewModel {
    private LiveData<User> user = UserModel.instance().getSignedUser();

    public LiveData<User> getUser(){
        return user;
    }

    public void addPost(Post post, Bitmap postImageBitmap) {
        PostModel.instance().uploadImage(post.getId(), postImageBitmap, url -> {
            setPostImage(post, url);
            PostModel.instance().addPost(post);
        });
    }

    private void setPostImage(Post post, String url) {
        if (url != null) {
            post.setImage(url);
        }
    }
}
