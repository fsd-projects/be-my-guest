package com.example.be_my_guest.viewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.be_my_guest.model.PostModel;
import com.example.be_my_guest.model.UserModel;
import com.example.be_my_guest.model.entities.User;
import com.example.be_my_guest.model.entities.relations.PostWithUser;

import java.util.List;

public class FeedViewModel extends ViewModel {
    private final LiveData<User> signedUser = UserModel.instance().getSignedUser();
    private final LiveData<List<PostWithUser>> postsWithUser = PostModel.instance().getAllPostsWithUser();

    public LiveData<List<PostWithUser>> getAllPostsWithUser() {
        return postsWithUser;
    }

    public LiveData<User> getSignedUser(){
        return signedUser;
    }
}
