package com.example.be_my_guest.viewModel;

import androidx.lifecycle.ViewModel;
import com.example.be_my_guest.model.UserModel;

public class MainViewModel extends ViewModel {

    public void signOutUser() {
        UserModel.instance().signUserOut();
    }

    public boolean isSignedIn() {
        return UserModel.instance().isSignedIn();
    }
}
