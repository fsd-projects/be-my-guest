package com.example.be_my_guest.viewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.be_my_guest.model.PostModel;
import com.example.be_my_guest.model.UserModel;
import com.example.be_my_guest.model.entities.Post;
import com.example.be_my_guest.model.entities.relations.PostWithUser;

import java.util.List;

public class MapViewModel extends ViewModel {
    private LiveData<List<PostWithUser>> postsWithUser = PostModel.instance().getAllPostsWithUser();

    public LiveData<List<PostWithUser>> getAllPostsWithUser() {
        return postsWithUser;
    }
}
