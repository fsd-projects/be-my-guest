package com.example.be_my_guest.viewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.be_my_guest.model.PostModel;
import com.example.be_my_guest.model.UserModel;
import com.example.be_my_guest.model.entities.Post;
import com.example.be_my_guest.model.entities.User;

import java.util.List;

public class UserProfilePostListViewModel extends ViewModel {
    private final LiveData<List<Post>> userPosts = PostModel.instance().getAllUserPosts();
    private final LiveData<User> user = UserModel.instance().getSignedUser();

    public LiveData<List<Post>> getUserPosts() {
        return userPosts;
    }

    public LiveData<User> getUser(){
        return user;
    }
}
