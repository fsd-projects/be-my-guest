package com.example.be_my_guest.viewModel;

import androidx.lifecycle.ViewModel;

import com.example.be_my_guest.model.Model;
import com.example.be_my_guest.model.UserModel;
import com.example.be_my_guest.model.entities.User;

public class EditProfileViewModel extends ViewModel {
    public void updateUser(User user, Model.RefreshListener callback){
        UserModel.instance().updateUser(user, callback);
    }
}
