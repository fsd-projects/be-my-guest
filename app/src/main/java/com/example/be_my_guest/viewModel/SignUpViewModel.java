package com.example.be_my_guest.viewModel;

import androidx.lifecycle.ViewModel;

import com.example.be_my_guest.model.UserModel;

public class SignUpViewModel extends ViewModel {

    public void setSignedUser() {
        UserModel.instance().setSignedUser();
    }
}
