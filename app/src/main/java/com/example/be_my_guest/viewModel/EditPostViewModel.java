package com.example.be_my_guest.viewModel;

import androidx.lifecycle.ViewModel;

import com.example.be_my_guest.model.Model;
import com.example.be_my_guest.model.PostModel;
import com.example.be_my_guest.model.entities.Post;

public class EditPostViewModel extends ViewModel {
    public void updatePost(Post post){
        PostModel.instance().updatePost(post);
    }
}
