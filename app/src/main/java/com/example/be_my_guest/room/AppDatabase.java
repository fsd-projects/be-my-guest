package com.example.be_my_guest.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.be_my_guest.dao.PostDao;
import com.example.be_my_guest.dao.UserDao;
import com.example.be_my_guest.model.entities.Post;
import com.example.be_my_guest.model.entities.User;
import com.example.be_my_guest.utils.Converters;

@Database(entities = {User.class, Post.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

    private static final String DB_NAME = "BeMyGuest.db";
    private static volatile AppDatabase appDatabase;

    public static synchronized AppDatabase getInstance(Context context) {
        if (appDatabase == null) appDatabase = create(context);
        return appDatabase;
    }

    private static AppDatabase create(final Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, DB_NAME).build();
    }

    public abstract UserDao userDao();

    public abstract PostDao postDao();

}