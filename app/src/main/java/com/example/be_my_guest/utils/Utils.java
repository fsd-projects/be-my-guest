package com.example.be_my_guest.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class Utils {
    static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");;

    public static boolean validateEditTextFields(List<EditText> fields, Context context) {
        for (EditText field : fields) {
            if (TextUtils.isEmpty(field.getText())) {
                Toast.makeText(context, "Please fill " + field.getHint(),
                        Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }

    public static String toDateFormat(Date date) {
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        return dateFormat.format(date);
    }

    public static Date parseDateFormat(String date) {
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }
}
