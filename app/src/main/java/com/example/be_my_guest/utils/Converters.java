package com.example.be_my_guest.utils;

import androidx.room.TypeConverter;

import com.google.firebase.firestore.GeoPoint;
import com.google.gson.Gson;

import java.util.Date;

public class Converters {
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

    @TypeConverter
    public static GeoPoint stringToGeoPoint(String data) {
        return new Gson().fromJson(data, GeoPoint.class);
    }

    @TypeConverter
    public static String geoPointToString(GeoPoint geoPoint) {
        return new Gson().toJson(geoPoint);
    }
}