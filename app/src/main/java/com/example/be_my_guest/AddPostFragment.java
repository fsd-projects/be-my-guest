package com.example.be_my_guest;

import static com.example.be_my_guest.utils.Utils.parseDateFormat;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.be_my_guest.databinding.FragmentAddPostBinding;
import com.example.be_my_guest.model.entities.Post;
import com.example.be_my_guest.model.entities.User;
import com.example.be_my_guest.viewModel.AddPostViewModel;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.firebase.firestore.GeoPoint;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AddPostFragment extends Fragment {
    protected Executor executor = Executors.newSingleThreadExecutor();
    private final String MAPS_API_KEY = "AIzaSyAijg367MGf6vgY63GpT_v5DdHsAY9FoFc";
    private FragmentAddPostBinding binding;
    private AutocompleteSupportFragment autocompleteFragment;
    private Place selectedPlace = null;
    private Date selectedStartDate = null;
    private Date selectedEndDate = null;
    private GeoApiContext geoApiContext = null;
    private Context context;
    private ActivityResultLauncher<Void> cameraLauncher;
    private ActivityResultLauncher<String> galleryLauncher;
    private Boolean isUserPhotoUploaded = false;
    private CircularProgressIndicator progressIndicator;
    private AddPostViewModel addPostViewModel;
    private User signedUser;
    private Calendar calendar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = requireContext();

        if (!Places.isInitialized()) {
            Places.initialize(context, MAPS_API_KEY);
        }

        PlacesClient placesClient = Places.createClient(context);
        binding = FragmentAddPostBinding.inflate(inflater, container, false);
        init();

        return binding.getRoot();
    }

    public void init() {
        initAutoCompleteFragment();
        initDatePickers();
        initializeImageLaunchers();
        setImageClickListeners();
        progressIndicator = binding.addPostProgressIndicator;
        geoApiContext = new GeoApiContext.Builder().apiKey(MAPS_API_KEY).build();
        signedUser = AddPostFragmentArgs.fromBundle(getArguments()).getUser();
        createAddPostListener();
    }

    private void createAddPostListener() {
        binding.addPostButton.setOnClickListener(view -> {
            if (validateFields()) {
                binding.addPostButton.setEnabled(false);
                binding.addPostButton.setEnabled(false);
                progressIndicator.show();
                savePost();
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        addPostViewModel = new ViewModelProvider(this).get(AddPostViewModel.class);
    }

    private void savePost() {
        Post post = initializePost();
        Bitmap bitmapPostImage = ((BitmapDrawable) binding.postImage.getDrawable()).getBitmap();
        addPostViewModel.addPost(post, bitmapPostImage);
        binding.addPostButton.setEnabled(true);
        progressIndicator.hide();
        Navigation.findNavController(binding.getRoot()).popBackStack();
    }

    private Post initializePost() {
        Post post = createPost();
        binding.postImage.setDrawingCacheEnabled(true);
        binding.postImage.buildDrawingCache();

        return post;
    }

    private void initAutoCompleteFragment() {
        autocompleteFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        if (autocompleteFragment != null) {
            autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS));
            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onError(@NonNull Status status) {
                    Log.e("error", status.getStatusMessage());
                    Toast.makeText(context, "Something went wrong, Please try again or enter another address", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onPlaceSelected(@NonNull Place place) {
                    selectedPlace = place;
                }
            });
        }
    }

    private void initDatePickers() {
        calendar = Calendar.getInstance();
        binding.editTextStartDate.setOnClickListener(view -> {
            startDateClickListener(binding.editTextStartDate);
        });
        binding.editTextEndDate.setOnClickListener(view -> {
            endDateClickListener(binding.editTextEndDate);
        });
    }

    private void startDateClickListener(EditText dateEditText) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, android.R.style.Theme_Holo_Light_Dialog, (pickerView, pickedYear, pickedMonth, pickedDay) -> {
            dateEditText.setText(pickedDay + "/" + (pickedMonth + 1) + "/" + pickedYear);
            this.selectedStartDate = parseDateFormat(dateEditText.getText().toString());
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void endDateClickListener(EditText dateEditText) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, android.R.style.Theme_Holo_Light_Dialog, (pickerView, pickedYear, pickedMonth, pickedDay) -> {
            dateEditText.setText(pickedDay + "/" + (pickedMonth + 1) + "/" + pickedYear);
            this.selectedEndDate = parseDateFormat(dateEditText.getText().toString());
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private Post createPost() {
        GeocodingResult[] results = null;
        results = Geocode(selectedPlace.getAddress());

        if (results == null) {
            return null;
        } else {
            LatLng latLng = GetLatLngFromGeocodingResult(results[0]);

            return new Post(UUID.randomUUID().toString(), signedUser.getId(), "", binding.postDescriptionAddPost.getText().toString(), new GeoPoint(latLng.latitude, latLng.longitude), selectedPlace.getAddress(), new Date(), selectedStartDate, selectedEndDate);
        }
    }

    private LatLng GetLatLngFromGeocodingResult(GeocodingResult geocodingResult) {
        return new LatLng(geocodingResult.geometry.location.lat, geocodingResult.geometry.location.lng);
    }

    private GeocodingResult[] Geocode(String address) {
        GeocodingResult[] results = null;

        try {
            results = GeocodingApi.geocode(geoApiContext, address).await();
        } catch (IOException | InterruptedException | ApiException e) {
            Toast.makeText(context, "could not add post, please try again", Toast.LENGTH_LONG).show();
            Log.e("Error", e.getMessage());
        }

        return results;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        executor.execute(() -> {
            geoApiContext.shutdown();
        });
    }

    private void setImageClickListeners() {
        binding.addPostUploadFromCamera.setOnClickListener(view1 -> {
            cameraLauncher.launch(null);
        });

        binding.addPostUploadFromGallery.setOnClickListener(view1 -> {
            galleryLauncher.launch("image/*");
        });
    }

    private void initializeImageLaunchers() {
        cameraLauncher = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), new ActivityResultCallback<Bitmap>() {
            @Override
            public void onActivityResult(Bitmap result) {
                if (result != null) {
                    binding.postImage.setImageBitmap(result);
                    isUserPhotoUploaded = true;
                }
            }
        });
        galleryLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
            @Override
            public void onActivityResult(Uri result) {
                if (result != null) {
                    binding.postImage.setImageURI(result);
                    isUserPhotoUploaded = true;
                }
            }
        });
    }

    private boolean validateFields() {
        boolean allFieldsValid = true;
        String validationErrorMessage = "Post invalid! Missing fields: ";

        if (!isUserPhotoUploaded) {
            allFieldsValid = false;
            validationErrorMessage += "\nImage,";
        }

        if (binding.postDescriptionAddPost.getText().toString().equals("")) {
            allFieldsValid = false;
            validationErrorMessage += "\nDescription,";
        }

        if (selectedPlace == null) {
            allFieldsValid = false;
            validationErrorMessage += "\nAddress,";
        }

        if (selectedStartDate == null) {
            allFieldsValid = false;
            validationErrorMessage += "\nStart Date,";
        }

        if (selectedEndDate == null) {
            allFieldsValid = false;
            validationErrorMessage += "\nEnd date,";
        } else if (selectedStartDate != null && selectedEndDate.before(selectedStartDate)) {
            if (allFieldsValid) {
                allFieldsValid = false;
                validationErrorMessage = "End date cannot be earlier than start date.";
            } else {
                validationErrorMessage += " End date cannot be earlier than start date.";
            }
        }

        if (!allFieldsValid) {
            validationErrorMessage += " Please fill them and try again";
            Toast.makeText(context, validationErrorMessage, Toast.LENGTH_LONG).show();
        }

        return allFieldsValid;
    }
}