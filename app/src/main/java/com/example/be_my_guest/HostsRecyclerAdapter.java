package com.example.be_my_guest;

import static com.example.be_my_guest.utils.Utils.toDateFormat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.be_my_guest.model.entities.relations.PostWithUser;
import com.squareup.picasso.Picasso;

import java.util.List;

class HostViewHolder extends RecyclerView.ViewHolder {
    private ImageView postImage;
    private ImageView userImage;
    private TextView userName;
    private TextView description;
    private TextView postDate;
    private TextView datesAvailable;
    private TextView address;

    public HostViewHolder(@NonNull View itemView) {
        super(itemView);
        this.postImage = itemView.findViewById(R.id.host_list_row_image);
        this.description = itemView.findViewById(R.id.host_list_row_details);
        this.userName = itemView.findViewById(R.id.host_list_row_username);
        this.userImage = itemView.findViewById(R.id.host_list_row_user_image);
        this.postDate = itemView.findViewById(R.id.host_list_row_post_date);
        this.datesAvailable = itemView.findViewById(R.id.host_list_row_available_dates);
        this.address = itemView.findViewById(R.id.host_list_row_address);
    }

    public void bind(PostWithUser post) {
        bindImages(post);
        this.description.setText(post.post.getDescription());
        this.userName.setText(String.format("%s %s", post.user.getFirstName(),
                post.user.getLastName()));
        this.postDate.setText(toDateFormat(post.post.getPostTime()));
        this.datesAvailable.setText(String.format("%s - %s",
                toDateFormat(post.post.getStartDate()), toDateFormat(post.post.getEndDate())));
        this.address.setText(post.post.getAddress());
        setOnClickListener(post);
    }

    private void bindImages(PostWithUser post) {
        Picasso.get().load(post.post.getImage()).into(postImage);
        Picasso.get().load(post.user.getPhotoUrl()).into(userImage);
    }

    private void setOnClickListener(PostWithUser postWithUser) {
        itemView.setOnClickListener(view -> Navigation.findNavController(itemView)
                .navigate(FeedFragmentDirections.actionFeedFragmentToPostFragment(postWithUser)));
    }
}

public class HostsRecyclerAdapter extends RecyclerView.Adapter<HostViewHolder> {
    LayoutInflater inflater;
    List<PostWithUser> data;

    public HostsRecyclerAdapter(LayoutInflater inflater, List<PostWithUser> data) {
        this.inflater = inflater;
        this.data = data;
    }

    public void setPostsList(List<PostWithUser> postsList) {
        this.data = postsList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(final int position) {
        return R.layout.host_list_row;
    }

    @NonNull
    @Override
    public HostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new HostViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HostViewHolder holder, int position) {
        if (!data.isEmpty()) {
            PostWithUser post = data.get(position);
            holder.bind(post);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
