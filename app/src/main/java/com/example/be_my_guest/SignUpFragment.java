package com.example.be_my_guest;

import static android.content.ContentValues.TAG;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.be_my_guest.databinding.FragmentSignUpBinding;
import com.example.be_my_guest.model.UserModel;
import com.example.be_my_guest.model.entities.User;
import com.example.be_my_guest.utils.Utils;
import com.example.be_my_guest.viewModel.SignUpViewModel;
import com.google.android.gms.tasks.Task;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class SignUpFragment extends Fragment {
    private FirebaseAuth mAuth;
    private DatePickerDialog picker;
    private Date pickedDate;
    private EditText firstNameEdit;
    private EditText lastNameEdit;
    private EditText emailEdit;
    private EditText passwordEdit;
    private EditText phoneNumberEdit;
    private EditText dateEdit;
    private ImageView userPhoto;
    private FragmentSignUpBinding binding;
    private ActivityResultLauncher<Void> cameraLauncher;
    private ActivityResultLauncher<String> galleryLauncher;
    private Boolean isUserPhotoUploaded = false;
    private CircularProgressIndicator progressIndicator;
    private SignUpViewModel signUpViewModel;
    private Button signUpButton;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSignUpBinding.inflate(inflater, container, false);
        initializeImageLaunchers();

        return binding.getRoot();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        signUpViewModel = new ViewModelProvider(this).get(SignUpViewModel.class);
    }

    private void initializeImageLaunchers() {
        cameraLauncher = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), new ActivityResultCallback<Bitmap>() {
            @Override
            public void onActivityResult(Bitmap result) {
                if (result != null) {
                    binding.signUpPhoto.setImageBitmap(result);
                    isUserPhotoUploaded = true;
                }
            }
        });
        galleryLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(), result -> {
            if (result != null) {
                binding.signUpPhoto.setImageURI(result);
                isUserPhotoUploaded = true;
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeField();
        setSignUpClickListener();
        setDateClickListener();
        setImageClickListeners();
    }

    private void setImageClickListeners() {
        binding.signUpUploadFromCamera.setOnClickListener(view1 -> {
            cameraLauncher.launch(null);
        });

        binding.signUpUploadFromGallery.setOnClickListener(view1 -> {
            galleryLauncher.launch("image/*");
        });
    }

    private void initializeField() {
        firstNameEdit = binding.signUpFirstName;
        lastNameEdit = binding.signUpLastName;
        emailEdit = binding.signUpEmail;
        passwordEdit = binding.signUpPassword;
        phoneNumberEdit = binding.signUpPhoneNumber;
        dateEdit = binding.signUpDateOfBirth;
        userPhoto = binding.signUpPhoto;
        progressIndicator = binding.signUpProgressIndicator;
        signUpButton = binding.signUpNextButton;
        mAuth = FirebaseAuth.getInstance();
    }

    private void setDateClickListener() {
        dateEdit.setOnClickListener(view -> {
            final Calendar calendar = Calendar.getInstance();
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);

            picker = new DatePickerDialog(getContext(), android.R.style.Theme_Holo_Light_Dialog,
                    (pickerView, pickedYear, pickedMonth, pickedDay) -> dateEdit.setText(
                            pickedDay + "/" + (pickedMonth + 1) + "/" + pickedYear), year, month, day);
            pickedDate = calendar.getTime();
            picker.show();
        });
    }

    private void setSignUpClickListener() {
        binding.signUpNextButton.setOnClickListener(view -> {
            if (isImageUploaded() && Utils.validateEditTextFields(Arrays.asList(firstNameEdit, lastNameEdit, emailEdit, passwordEdit, phoneNumberEdit, dateEdit), getContext())) {
                signUpButton.setEnabled(false);
                progressIndicator.show();
                SignUserUp();
            }
        });
    }

    private Boolean isImageUploaded() {
        if (!isUserPhotoUploaded) {
            Toast.makeText(getContext(), "Please upload a profile image",
                    Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @NonNull
    private User createUser(FirebaseUser firebaseUser) {

        return new User(firebaseUser.getUid(), firstNameEdit.getText().toString(), lastNameEdit.getText().toString(),
                emailEdit.getText().toString(), phoneNumberEdit.getText().toString(),
                pickedDate, "");
    }

    private void SignUserUp() {
        mAuth.createUserWithEmailAndPassword(emailEdit.getText().toString(), passwordEdit.getText().toString())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        signUp();
                    } else {
                        showSignUpFailure(task);
                    }
                });
    }

    private void showSignUpFailure(Task<AuthResult> task) {
        Log.w(TAG, "createUserWithEmail:failure", task.getException());
        Toast.makeText(getContext(), "Authentication failed.",
                Toast.LENGTH_SHORT).show();
        progressIndicator.hide();
        signUpButton.setEnabled(true);
    }

    private void signUp() {
        Log.d(TAG, "createUserWithEmail:success");
        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        saveUserInDB(firebaseUser);
        signUpViewModel.setSignedUser();
        startActivity(new Intent(MyApplication.getMyContext(), StartUpActivity.class));
    }

    private void saveUserInDB(FirebaseUser firebaseUser) {
        assert firebaseUser != null;
        User user = createUser(firebaseUser);
        Bitmap bitmap = getPhoto();

        UserModel.instance().uploadImage(user.getEmail(), bitmap, url -> addUser(user, url));
    }

    private void addUser(User user, String url) {
        setUserPhoto(user, url);
        UserModel.instance().addUser(user, () -> {});
    }

    private void setUserPhoto(User user, String url) {
        if (url != null) {
            user.setPhotoUrl(url);
        }
    }

    private Bitmap getPhoto() {
        userPhoto.setDrawingCacheEnabled(true);
        userPhoto.buildDrawingCache();

        return ((BitmapDrawable) userPhoto.getDrawable()).getBitmap();
    }
}