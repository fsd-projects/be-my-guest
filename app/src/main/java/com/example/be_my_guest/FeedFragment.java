package com.example.be_my_guest;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.be_my_guest.databinding.FragmentFeedBinding;
import com.example.be_my_guest.model.entities.User;
import com.example.be_my_guest.model.entities.relations.PostWithUser;
import com.example.be_my_guest.viewModel.FeedViewModel;
import com.google.android.material.progressindicator.CircularProgressIndicator;

import java.util.LinkedList;
import java.util.List;

public class FeedFragment extends Fragment {
    private NavController navController;
    private RecyclerView recyclerView;
    private FragmentFeedBinding binding;
    private List<PostWithUser> postsList = new LinkedList<>();
    private HostsRecyclerAdapter adapter;
    private FeedViewModel feedViewModel;
    private CircularProgressIndicator progressIndicator;
    private User signedUser;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentFeedBinding.inflate(inflater, container, false);
        binding.postsList.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new HostsRecyclerAdapter(getLayoutInflater(), postsList);
        binding.postsList.setAdapter(adapter);
        progressIndicator = binding.feedProgressIndicator;

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = NavHostFragment.findNavController(this);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);
        initializeMembers();
        initializeRecycleView();
        initializeListeners();
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        observePosts();
    }

    private void initializeMembers() {
        recyclerView = binding.postsList;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        feedViewModel = new ViewModelProvider(this).get(FeedViewModel.class);
    }

    private void initializeRecycleView() {
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new HostsRecyclerAdapter(getLayoutInflater(), postsList);
        observePosts();
        recyclerView.setAdapter(adapter);
    }

    private void initializeListeners() {
        binding.addPostFab.setOnClickListener((view) -> navController.navigate(FeedFragmentDirections.actionFeedFragmentToAddPostFragment(signedUser)));
        binding.viewOnMapFab.setOnClickListener((view) -> navController.navigate(R.id.action_feed_fragment_to_map_fragment));
        binding.userProfileFab.setOnClickListener((view) -> navController.navigate(FeedFragmentDirections.actionFeedFragmentToUserProfileFragment(signedUser)));
    }

    private void observePosts() {
        progressIndicator.show();
        feedViewModel.getSignedUser().observe(getViewLifecycleOwner(), user -> {
            signedUser = user;
            feedViewModel.getAllPostsWithUser().observe(getViewLifecycleOwner(), this::setPosts);
        });
    }

    private void setPosts(List<PostWithUser> posts) {
        postsList = posts;
        progressIndicator.hide();
        adapter.setPostsList(postsList);
    }
}
