package com.example.be_my_guest.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.example.be_my_guest.model.entities.Post;
import com.example.be_my_guest.model.entities.relations.PostWithUser;

import java.util.List;

@Dao
public interface PostDao {
    @Query("SELECT * FROM Post")
    List<Post> getAll();

    @Transaction
    @Query("SELECT * FROM Post")
    List<PostWithUser> getPostsWithUser();

    @Transaction
    @Query("SELECT * FROM Post WHERE userId=:userId")
    List<Post> getPostsByUserId(String userId);

    @Insert
    void insert(Post post);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Post... posts);

    @Update
    void update(Post post);
}