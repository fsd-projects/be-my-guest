package com.example.be_my_guest;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.be_my_guest.databinding.FragmentUserProfileBinding;
import com.example.be_my_guest.model.entities.Post;
import com.example.be_my_guest.model.entities.User;
import com.example.be_my_guest.viewModel.UserProfilePostListViewModel;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;
import java.util.List;

public class UserProfileFragment extends Fragment {
    private TextView userName;
    private ImageView userPhoto;
    private RecyclerView recyclerView;
    private UserProfilePostListViewModel userProfileViewModel;
    private List<Post> userPosts = new LinkedList<>();
    private User signedUser;
    private FragmentUserProfileBinding binding;
    private UserProfilePostRecyclerAdapter adapter;
    private CircularProgressIndicator progressIndicator;
    private NavController navController;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentUserProfileBinding.inflate(inflater, container, false);
        binding.userProfileFragmentPostsList.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new UserProfilePostRecyclerAdapter(getLayoutInflater(), userPosts);
        binding.userProfileFragmentPostsList.setAdapter(adapter);
        progressIndicator = binding.userProfileProgressIndicator;
        initializeSignedUser();

        return binding.getRoot();
    }

    private void initializeSignedUser() {
        if (signedUser == null) {
            signedUser = UserProfileFragmentArgs.fromBundle(getArguments()).getUser();
        } else {
            updateSignedUser();
        }
    }

    private void updateSignedUser() {
        userProfileViewModel.getUser().observe(getViewLifecycleOwner(), user -> {
            signedUser = user;
            initializeUserData();
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        initializeMembers(view);
        initializeRecycleView();
        setEditProfileListener();
    }

    private void initializeUserData() {
        initializeUserDetails();
        observeUserPosts();
    }

    private void setEditProfileListener() {
        binding.profileFragmentEditUser.setOnClickListener(view ->
                navController.navigate(UserProfileFragmentDirections.actionUserProfileFragmentToEditProfileFragment(signedUser)));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        userProfileViewModel = new ViewModelProvider(this).get(UserProfilePostListViewModel.class);
    }

    private void initializeMembers(View view) {
        navController = Navigation.findNavController(view);
        userName = binding.profileFragmentUserName;
        userPhoto = binding.profileFragmentPhoto;
        recyclerView = binding.userProfileFragmentPostsList;
        initializeUserDetails();
    }

    private void initializeUserDetails() {
        userName.setText(String.format("%s %s", signedUser.getFirstName(), signedUser.getLastName()));
        Picasso.get()
                .load(signedUser.getPhotoUrl())
                .into(userPhoto);
    }

    private void initializeRecycleView() {
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new UserProfilePostRecyclerAdapter(getLayoutInflater(), userPosts);
        observeUserPosts();
        recyclerView.setAdapter(adapter);
    }

    private void observeUserPosts() {
        progressIndicator.show();
        binding.fragmentProfileMyPostsChip.setText(R.string.loading_posts);
        userProfileViewModel.getUserPosts().observe(getViewLifecycleOwner(), this::setPosts);
    }

    private void setPosts(List<Post> posts) {
        userPosts = posts;
        updateChipText();
        progressIndicator.hide();
        adapter.setPostsList(userPosts);
    }

    private void updateChipText() {
        if (userPosts.isEmpty()) {
            binding.fragmentProfileMyPostsChip.setText(R.string.no_posts);
        } else {
            binding.fragmentProfileMyPostsChip.setText(R.string.my_posts);
        }
    }
}