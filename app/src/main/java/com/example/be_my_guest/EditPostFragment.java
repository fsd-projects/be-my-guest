package com.example.be_my_guest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.be_my_guest.databinding.FragmentEditPostBinding;
import com.example.be_my_guest.model.PostModel;
import com.example.be_my_guest.model.entities.Post;
import com.example.be_my_guest.viewModel.EditPostViewModel;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.squareup.picasso.Picasso;

public class EditPostFragment extends Fragment {
    private Post post;
    private EditText postDescription;
    private ImageView postImage;
    private CircularProgressIndicator progressIndicator;
    private ActivityResultLauncher<Void> cameraLauncher;
    private ActivityResultLauncher<String> galleryLauncher;
    private EditPostViewModel editPostViewModel;
    private FragmentEditPostBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEditPostBinding.inflate(inflater, container, false);
        initializeImageLaunchers();

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializePost(EditPostFragmentArgs.fromBundle(getArguments()).getPost());

        setImageClickListeners();
        setSaveButtonListener();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        editPostViewModel = new ViewModelProvider(this).get(EditPostViewModel.class);
    }

    private void initializePost(Post post) {
        this.post = post;
        postDescription = binding.editPostDescription;
        postImage = binding.editPostImage;
        progressIndicator = binding.editPostProgressIndicator;
        postDescription.setText(post.getDescription());
        Picasso.get().load(post.getImage()).into(postImage);
    }

    private void setSaveButtonListener() {
        binding.editPostSaveButton.setOnClickListener(view1 -> {
            binding.editPostSaveButton.setEnabled(false);
            progressIndicator.show();
            updatePost();
        });
    }

    private void updatePost() {
        Post newPost = new Post(post);
        newPost.setDescription(postDescription.getText().toString());
        Bitmap postPhoto = ((BitmapDrawable) postImage.getDrawable()).getBitmap();

        PostModel.instance().uploadImage(newPost.getId(), postPhoto, url -> {
            setImageUrl(newPost, url);
            updateNewPost(newPost);
        });
    }

    private void setImageUrl(Post newPost, String url) {
        if (url != null) {
            newPost.setImage(url);
        }
    }

    private void updateNewPost(Post newPost) {
        editPostViewModel.updatePost(newPost);
        binding.editPostSaveButton.setEnabled(true);
        progressIndicator.hide();
        Navigation.findNavController(binding.getRoot()).popBackStack();
    }

    private void setImageClickListeners() {
        binding.editPostUploadFromCamera.setOnClickListener(view -> cameraLauncher.launch(null));
        binding.editPostUploadFromGallery.setOnClickListener(view -> galleryLauncher.launch("image/*"));
    }

    private void initializeImageLaunchers() {
        cameraLauncher = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), result -> {
            if (result != null) {
                binding.editPostImage.setImageBitmap(result);
            }
        });
        galleryLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(), result -> {
            if (result != null) {
                binding.editPostImage.setImageURI(result);
            }
        });
    }
}