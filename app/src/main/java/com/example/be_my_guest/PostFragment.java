package com.example.be_my_guest;

import static com.example.be_my_guest.utils.Utils.toDateFormat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.example.be_my_guest.databinding.FragmentPostBinding;
import com.example.be_my_guest.model.entities.relations.PostWithUser;
import com.squareup.picasso.Picasso;

public class PostFragment extends Fragment {
    private FragmentPostBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentPostBinding.inflate(inflater, container, false);
        initClose();
        initDetails();

        return binding.getRoot();
    }

    private void initClose() {
        binding.closeButton.setOnClickListener(view -> {
            NavController navController = NavHostFragment.findNavController(this);
            navController.popBackStack();
        });
    }

    private void initDetails() {
        PostWithUser currentPost = PostFragmentArgs.fromBundle(getArguments()).getPostWithUser();
        bind(currentPost);
    }

    private void bind(PostWithUser currentPost) {
        binding.userFullNameMapPost.setText(
                String.format("%s %s", currentPost.user.getFirstName(), currentPost.user.getLastName()));
        binding.addressMapPostValue.setText(currentPost.post.getAddress());
        binding.phoneMapPostValue.setText(currentPost.user.getPhoneNumber());
        binding.descriptionMapPostValue.setText(currentPost.post.getDescription());
        binding.emailMapPostValue.setText(currentPost.user.getEmail());
        initializeDates(currentPost);
        Picasso.get().load(currentPost.post.getImage()).into(binding.imageMapPost);
    }

    private void initializeDates(PostWithUser currentPost) {
        String startDate = toDateFormat(currentPost.post.getStartDate());
        String endDate = toDateFormat(currentPost.post.getEndDate());
        binding.datesMapPostValue.setText(
                String.format("%s - %s", startDate, endDate));
    }
}