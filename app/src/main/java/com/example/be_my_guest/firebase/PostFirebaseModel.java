package com.example.be_my_guest.firebase;

import com.example.be_my_guest.model.Model;
import com.example.be_my_guest.model.entities.Post;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class PostFirebaseModel extends FirebaseModel {

    public PostFirebaseModel() {
        super();
    }

    public void getAllPostsSince(Long since, Model.Listener<List<Post>> callback) {
        db.collection(Post.COLLECTION)
                .whereGreaterThanOrEqualTo(Post.LAST_UPDATED, new Timestamp(since, 0))
                .get()
                .addOnCompleteListener(task -> {
                    List<Post> list = new LinkedList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot jsonList = task.getResult();
                        for (DocumentSnapshot json : jsonList) {
                            list.add(Post.fromJson(Objects.requireNonNull(json.getData())));
                        }
                    }
                    callback.onComplete(list);
                });
    }

    public void addPost(Post post, Model.RefreshListener listener) {
        db.collection(Post.COLLECTION).document(post.getId()).set(post.toJson())
                .addOnCompleteListener(task -> listener.onComplete());
    }

    public void updatePost(Post post, Model.RefreshListener callback) {
        Map<String, Object> jsonUser = post.toJson();
        db.collection("Posts")
                .document(post.getId())
                .update(jsonUser)
                .addOnSuccessListener(unused -> callback.onComplete());
    }
}
