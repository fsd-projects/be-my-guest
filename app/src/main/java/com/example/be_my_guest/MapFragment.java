package com.example.be_my_guest;

import static android.content.Context.LOCATION_SERVICE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.be_my_guest.databinding.FragmentMapBinding;
import com.example.be_my_guest.model.entities.Post;
import com.example.be_my_guest.model.entities.relations.PostWithUser;
import com.example.be_my_guest.viewModel.MapViewModel;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MapFragment extends Fragment {
    final String[] PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};
    private Context context;
    private NavController navController;
    private ActivityResultLauncher<String[]> multiplePermissionLauncher;
    private Location lastKnownLocation;
    private LocationManager locationManager;
    private FragmentMapBinding binding;
    private MapView mapView;
    private IMapController mapController;
    private boolean showUsersCurrentLocation = false;

    private List<PostWithUser> postsList = new LinkedList<>();

    private MapViewModel mapViewModel;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mapViewModel = new ViewModelProvider(this).get(MapViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = requireContext();
        Configuration.getInstance().load(context, PreferenceManager.getDefaultSharedPreferences(context));
        HandlePermissions(context);
        binding = FragmentMapBinding.inflate(inflater, container, false);
        observePosts();
        initMap(context);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = NavHostFragment.findNavController(this);
    }

    private void observePosts() {
        mapViewModel.getAllPostsWithUser().observe(getViewLifecycleOwner(), posts -> {
            postsList = posts;
            addPostsIcons(posts);
        });
    }

    private void HandlePermissions(Context context) {
        ActivityResultContracts.RequestMultiplePermissions multiplePermissionsContract =
                new ActivityResultContracts.RequestMultiplePermissions();
        multiplePermissionLauncher = registerForActivityResult(multiplePermissionsContract, isGranted -> {
            if (isGranted.containsValue(false)) {
                multiplePermissionLauncher.launch(PERMISSIONS);
            } else {
                startLocationHandling(context);
            }
        });

        checkSelfPermissions(context, multiplePermissionLauncher);
    }

    private void checkSelfPermissions(Context context, ActivityResultLauncher<String[]> multiplePermissionLauncher) {
        for (String permission : PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(context, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                multiplePermissionLauncher.launch(PERMISSIONS);
            } else {
                startLocationHandling(context);
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void startLocationHandling(Context context) {
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        boolean isLocationEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!isLocationEnabled) {
            askUserToEnableLocationServices(context);
        }

        String provider = locationManager.getBestProvider(createCriteria(), true);

        if (provider != null) {
            lastKnownLocation = getLastKnownLocation();
            showUsersCurrentLocation = true;

            if (mapController != null) {
                CenterOnMyLocation();
            }
        }
    }

    private Location getLastKnownLocation() {
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;

        for (String provider : providers) {
            @SuppressLint("MissingPermission") Location location =
                    locationManager.getLastKnownLocation(provider);

            if (location == null) {
                continue;
            }

            if (bestLocation == null || location.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = location;
            }
        }

        return bestLocation;
    }

    private Criteria createCriteria() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_MEDIUM);

        return criteria;
    }

    private void askUserToEnableLocationServices(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Location Service Disabled");
        builder.setMessage("Please enable location service");
        builder.setPositiveButton("Enable Location Service", (dialog, which) -> {
            Intent enableLocationIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(enableLocationIntent);
        });
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    private void addPostsIcons(List<PostWithUser> postsWithUser) {
        List<OverlayItem> items = createOverlayItems(postsWithUser);

        // Although it's deprecated, there is no alternative...
        ItemizedOverlayWithFocus<OverlayItem> overlay = new ItemizedOverlayWithFocus<OverlayItem>(context,
                items,
                new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
                    @Override
                    public boolean onItemSingleTapUp(int index, OverlayItem item) {
                        PostWithUser postClicked = postsList.stream().filter(post ->
                                post.post.getId().equals(item.getTitle())
                        ).findFirst().get();
                        navController.navigate(MapFragmentDirections.actionMapFragmentToPostFragment(postClicked));

                        return true;
                    }

                    @Override
                    public boolean onItemLongPress(int index, OverlayItem item) {
                        return false;
                    }
                });

        mapView.getOverlays().add(overlay);
    }

    private List<OverlayItem> createOverlayItems(List<PostWithUser> postsWithUser) {
        List<OverlayItem> items = new LinkedList<>();
        postsWithUser.forEach(postWithUser -> {
            // We put the post id as the item title so we can get the post later
            items.add(new OverlayItem(postWithUser.post.getId(),
                    postWithUser.post.getDescription(),
                    convertGeoPoint(postWithUser.post.getGeoPoint())));
        });

        return items;
    }

    private IGeoPoint convertGeoPoint(com.google.firebase.firestore.GeoPoint geoPoint) {
        return new GeoPoint(geoPoint.getLatitude(), geoPoint.getLongitude());
    }

    private void initMap(Context context) {
        mapView = binding.mapView;
        mapView.setTileSource(TileSourceFactory.MAPNIK);
        mapView.setMultiTouchControls(true);
        mapController = mapView.getController();
        mapController.setZoom(9.5);

        if (showUsersCurrentLocation) {
            addMyLocationToMap(context);
        } else {
            mapController.setCenter(new GeoPoint(32.069234, 34.991038));
        }
    }

    private void addMyLocationToMap(Context context) {
        MyLocationNewOverlay locationOverlay =
                new MyLocationNewOverlay(new GpsMyLocationProvider(context), mapView);
        locationOverlay.enableMyLocation();
        mapView.getOverlays().add(locationOverlay);
        CenterOnMyLocation();
    }

    private void CenterOnMyLocation() {
        if (lastKnownLocation != null) {
            GeoPoint myLocationGeoPoint =
                    new GeoPoint(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
            mapController.setCenter(myLocationGeoPoint);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }
}